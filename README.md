# Fakeflix

Fakeflix est un clone de Netflix, développé en React. Le site récupère les données de l'API de The Movie Database. Il affiche aussi les trailers Youtube de ces films.

Ce projet a été développé dans le cadre de la formation IUT Informatique à l'université d'Orléans durant l'année 2021.

Nous avons eu quelques soucis avec un ancien dépot, c'est pour cela que nos commits n'apparaissent pas.

## Auteurs

Quentin DA SILVA <br/>
Bruno GOMES

# Démonstration

Le site est disponible à l'adresse suivante: <br/> https://fakeflix-6b5b5.web.app/

# Lancer le projet

Pour lancer le projet, il faut lancer les commandes :

### `npm start`

Cette commande devrait lancer le serveur de développement

### `npm run build`

Cette commande construit l'application en récupérant les bundles de React.

L'application est prête à être utilisé !

## Bundles utilisés

React Youtube : https://www.npmjs.com/package/react-youtube <br/>
Movie Trailer : https://www.npmjs.com/package/movie-trailer <br/>
Axios : https://www.npmjs.com/package/axios

## Liens

Gitlab : https://gitlab.com/Saylux/fakeflix <br/>
TMDB : https://www.themoviedb.org/ <br/>
Documentation React : https://fr.reactjs.org/docs/getting-started.html

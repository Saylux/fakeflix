import React from 'react';
import './App.css';
import Row from './Row';
import Banner from './Banner';
import Navbar from './Navbar';
import requests from './requests';

function App() {
  return (
    <div className="app">

      <Navbar />

      <Banner />

      <Row title="Programme originaux Netflix" fetchUrl={requests.fetchNetflixOriginals} isLargeRow/>
      <Row title="Tendances actuelles" fetchUrl={requests.fetchTrending} />
      <Row title="Mieux notés" fetchUrl={requests.fetchTopRated} />
      <Row title="Film d'action" fetchUrl={requests.fetchActionMovies} />
      <Row title="Film de comédie" fetchUrl={requests.fetchComedyMovies} />
      <Row title="Film d'horreur" fetchUrl={requests.fetchHorrorMovies} />
      <Row title="Film de romance" fetchUrl={requests.fetchRomanceMovies} />
      <Row title="Fantastique" fetchUrl={requests.fetchAnimation} />
      <Row title="Science-Fiction" fetchUrl={requests.fetchSciFi} />
      <Row title="Thriller" fetchUrl={requests.fetchThriller} />
      <Row title="Documentaire" fetchUrl={requests.fetchDocumentaries} />


    </div>
  );
}

export default App;

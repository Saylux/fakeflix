import React, { useEffect, useState } from 'react'
import './Navbar.css';

function Navbar() {

    const [show, handleShow] = useState(false);

    // Fonction pour la rétractation de la navbar lors du scroll + transition de l'opacité.
    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.scrollY > 100) {
                handleShow(true);
            } else handleShow(false);
        });
        return () => {
            window.removeEventListener("scroll");
        };
    }, []);

    return (
        <div className={`nav ${show && "nav__black"}`}>
            <img
                className="nav__logo"
                src="fakeflix_logo.png"
                alt="fakeflix logo"
            />
            <img
                className="nav__avatar"
                src="https://upload.wikimedia.org/wikipedia/commons/0/0b/Netflix-avatar.png"
                alt="netflix logo"
            />
        </div>
    )
}

export default Navbar

import React, { useState, useEffect } from 'react';
import axios from './axios';
import './Row.css';
import Youtube from "react-youtube";
import movieTrailer from 'movie-trailer';

const base_url = "https://image.tmdb.org/t/p/original";

function Row({ title, fetchUrl, isLargeRow }) {
    // Variable ou l'on stock des informations
    const [movies, setMovies] = useState([]);
    const [trailerUrl, setTrailerUrl] = useState("");

    // Morceau de code qui se lance en fonction de condition/variable spécifiques
    useEffect(() => {
        async function fetchData(){
            const request = await axios.get(fetchUrl); // Création de l'url
            setMovies(request.data.results); // Récupération des infos de la request
            return request;
        }
        fetchData();
    }, [fetchUrl]); // Si [] => se lance quand la ligne se charge et ne la relance pas

    // Options pour la vidéo youtube
    const opts = {
        heigth: "390",
        width: "100%",
        playerVars: {
            autplay: 1
        },
    };

    // Fonction permettant l'affichage du trailer lorsqu'on clique sur une affiche
    const handlerClick = (movie) => {
        if(trailerUrl){
            setTrailerUrl("");
        }else{
            movieTrailer(movie?.name || movie?.title || movie?.original_name || "") // movieTrailer permet de faire la recherche sur le net
            .then((url) => {
                const urlParams = new URLSearchParams(new URL(url).search)
               setTrailerUrl(urlParams.get('v'));

            })
            .catch((error) => console.log(error))
        }
    }


    return (
        <div className="row">
            <h2 className="row__title">{title}</h2>
            <div className="row__posters">
                {movies.map(movie => (
                    <img 
                        key={movie.id}
                        onClick={() => handlerClick(movie)}
                        className={`row__poster ${isLargeRow && "row__posterLarge"}`}
                        src={`${base_url}${isLargeRow ? movie.poster_path : movie.backdrop_path}`} alt={movie.name}></img>
                ))} 
            </div>
            {trailerUrl && <Youtube videoId={trailerUrl} opts={opts} />}
        </div>
    )
}

export default Row